﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Geo.Measure;
using RurouniJones.OverlordBot.Cognitive.LanguageUnderstanding.Transmissions;
using RurouniJones.OverlordBot.Core.Responders;
using RurouniJones.OverlordBot.Core.Util;
using RurouniJones.OverlordBot.Datastore;
using RurouniJones.OverlordBot.Datastore.Models;

namespace RurouniJones.OverlordBot.Core.Awacs.IntentHandlers
{
    internal class LocationOfAirfieldHandler
    {
        internal record LocationOfAirfieldDetails : Reply.IReplyDetails
        {
            private readonly int _bearing;
            private readonly int _range;
            private readonly string _status;
            private readonly string _name;
            private readonly Reply.IReplyDetails.MeasurementSystem _system;

            public LocationOfAirfieldDetails(int bearing, int range, string status, string name, Reply.IReplyDetails.MeasurementSystem system)
            {
                _bearing = bearing;
                _range = range;
                _status = status;
                _name = name;
                _system = system;
            }

            public string ToSpeech()
            {
                var sections = new List<string?>
                {
                    _status,
                    Cognitive.SpeechOutput.AirbasePronouncer.PronounceAirbase(_name),
                    $"bearing {Reply.IReplyDetails.BearingToWords(_bearing)}",
                    Reply.IReplyDetails.RangeToWords(_range, _system)
                };
                sections.RemoveAll(i => i == null);

                return string.Join(", ", sections);
            }

            public string ToText()
            {
                return "TODO";
            }

        }

        private readonly IAirfieldRepository _airfieldRepository;

        public LocationOfAirfieldHandler(IAirfieldRepository airfieldRepository)
        {
            _airfieldRepository = airfieldRepository;
        }

        public async Task<Reply> Process(LocationOfEntityTransmission transmission, Unit transmitterUnit, Reply reply, string serverShortName)
        {
            var airfieldName = ((ITransmission.Airfield) transmission.Subject).Name;
            Unit airfield;
            if (airfieldName.Equals("nearest", StringComparison.CurrentCultureIgnoreCase))
                airfield = await _airfieldRepository.FindByClosest(transmitterUnit.Location, transmitterUnit.Coalition,
                    serverShortName);
            else
                airfield = await _airfieldRepository.FindByName(airfieldName, serverShortName);

            if (airfield == null)
            {
                reply.Message = $"I could not find {airfieldName}";
            }
            else
            {
                reply.Details = BuildResponse(transmitterUnit, airfield);
            }

            return reply;
        }

        private static LocationOfAirfieldDetails BuildResponse(Unit transmitterUnit, Unit subjectUnit)
        {
            var transmitterPoint = transmitterUnit.Location;

            var bearing = (int) Geospatial.TrueToMagnetic(transmitterPoint, transmitterUnit.BearingTo(subjectUnit));
            var range = (int) transmitterUnit.DistanceTo(subjectUnit, DistanceUnit.Km);

            string status = null;
            if (subjectUnit.Coalition == 0)
                status = "Neutral airfield";
            else if (transmitterUnit.Coalition != subjectUnit.Coalition)
                status = "Hostile airfield";

            var system = BogeyDopeHandler.MetricAirframes.Contains(transmitterUnit.Name)
                ? Reply.IReplyDetails.MeasurementSystem.Metric
                : Reply.IReplyDetails.MeasurementSystem.Imperial;

            return new LocationOfAirfieldDetails(bearing, range, status, subjectUnit.Name, system);
        }
    }
}
