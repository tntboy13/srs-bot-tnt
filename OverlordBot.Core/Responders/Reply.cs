﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Humanizer;
using RurouniJones.OverlordBot.Datastore.Models;
using RurouniJones.OverlordBot.Encyclopedia;

namespace RurouniJones.OverlordBot.Core.Responders
{
    public record Reply
    {
        public string CallerCallsign { get; set; }
        public string ResponderCallsign { get; set; }
        public string Message { get; set; }
        public IReplyDetails Details { get; set; }
        public string Notes { get; set; }

        public bool ContinueProcessing { get; set; }

        public Reply()
        {
            CallerCallsign = null;
            ResponderCallsign = null;
            Message = null;
            Details = null;
            ContinueProcessing = true;
            Notes = null;
        }

        public string ToSpeech()
        {
            if (Details != null)
            {
                var sections = new List<string> { CallerCallsign, ResponderCallsign, Details.ToSpeech() };
                sections.RemoveAll(i => i == null);
                return string.Join(", ", sections );
            }
            if (Message != null)
            {
                var sections = new List<string> { CallerCallsign, ResponderCallsign, Message };
                sections.RemoveAll(i => i == null);
                return string.Join(", ", sections);
            }

            return null;
        }

        public interface IReplyDetails
        {
            enum MeasurementSystem
            {
                Imperial,
                Metric
            }

            protected static string BearingToWords(int bearing)
            {
                return Regex.Replace(bearing.ToString("000"), "\\d{1}", " $0")[1..];
            }

            protected static string RangeToWords(int range, MeasurementSystem system)
            {
                var converted = system == MeasurementSystem.Metric
                    ? range
                    : (int) (range / 1.852);
                return converted.ToString();
            }

            protected static string AltitudeToWords(int altitude, MeasurementSystem system)
            {
                int convertedAltitude;

                if (system == MeasurementSystem.Metric)
                {
                    convertedAltitude = (int) Math.Round(altitude / 100d, 0) * 100;
                }
                else
                {
                    convertedAltitude = (int) Math.Round(altitude * 3.28d / 1000d, 0) * 1000;
                }

                if (convertedAltitude == 0) convertedAltitude = 500;

                return convertedAltitude.ToWords(new System.Globalization.CultureInfo("en-US"));
            }

            protected static List<string> CommentsToWords(int altitude, int speed)
            {
                var comments = new List<string>();

                // 40,000ft
                if (altitude > 12192)
                {
                    comments.Add("high");
                }

                switch (speed)
                {
                    // 900 knots
                    case > 463:
                        comments.Add("very fast");
                        break;
                    // 600 knots
                    case > 308:
                        comments.Add("fast");
                        break;
                }

                return comments;
            }

            protected static List<string> UnitCountsToWords(List<Unit> group)
            {
                var unitCounts = new Dictionary<string, int>();

                foreach (var contact in group.OrderBy(unit =>Aircraft.GetNameByDcsCode(unit.Name)))
                    if (unitCounts.ContainsKey(Aircraft.GetCodeByDcsCode(contact.Name)))
                        unitCounts[Aircraft.GetCodeByDcsCode(contact.Name)] += 1;
                    else
                        unitCounts.Add(Aircraft.GetCodeByDcsCode(contact.Name), 1);

                var spokenCounts = new List<string>();

                foreach (var (key, value) in unitCounts.OrderBy(pair => pair.Value))
                {
                    spokenCounts.Add(value == 1
                        ? Aircraft.GetNameByDcsCode(key)
                        : $"{value}-ship {Aircraft.GetNameByDcsCode(key)}");
                }

                return spokenCounts;
            }

            string ToSpeech();

            string ToText();
        }

    }
}
