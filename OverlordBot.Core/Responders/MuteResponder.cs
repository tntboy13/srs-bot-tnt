﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System.Threading.Tasks;
using NLog;
using RurouniJones.OverlordBot.Cognitive.LanguageUnderstanding.Transmissions;

namespace RurouniJones.OverlordBot.Core.Responders
{
    public class MuteResponder : IResponder
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        public async Task<Reply> ProcessTransmission(ITransmission transmission)
        {
            await Task.Run(() =>
            {
                switch (transmission.Intent)
                {
                    case ITransmission.Intents.None:
                    case ITransmission.Intents.RadioCheck:
                    case ITransmission.Intents.BogeyDope:
                    case ITransmission.Intents.LocationOfEntity:
                    case ITransmission.Intents.SetWarningRadius:
                    case ITransmission.Intents.ReadyToTaxi:
                    case ITransmission.Intents.InboundToAirfield:
                    case ITransmission.Intents.Picture:
                    case ITransmission.Intents.Declare:
                        _logger.Info(
                            $"Language Understanding Result. Intent: {transmission.Intent}, Transmitter: {transmission.Transmitter}, Receiver: {transmission.Receiver}");
                        break;
                    default:
                        _logger.Warn(
                            $"Unknown Intent: {transmission.Intent}, Transmitter: {transmission.Transmitter}, Receiver: {transmission.Receiver}");
                        break;
                }
            });

            return null;
        }
    }
}