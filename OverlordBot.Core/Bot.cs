﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using NLog;
using RurouniJones.OverlordBot.Administration;
using RurouniJones.OverlordBot.Diagnostics;
using RurouniJones.OverlordBot.Discord;

namespace RurouniJones.OverlordBot.Core
{
    public class Bot
    {
        private readonly List<Task> _botTasks = new();

        private readonly List<GameServer> _gameServers = new();
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private readonly string _name;

        private readonly Configuration botConfig;
        private readonly GameServerFactory gameServerFactory;
        private CancellationToken StoppingToken;

        public Bot(IOptions<Configuration> configuration, GameServerFactory gameServerFactory)
        {
            botConfig = configuration.Value;
            this.gameServerFactory = gameServerFactory;

            // Configure the bot-level diagnostics
            Collector.BotName = _name = botConfig.Name;

            // Configure Azure

            var azure = botConfig.Azure;
            Cognitive.Configuration.ConfigureSpeech(azure.Region, azure.Speech.SubscriptionKey,
                azure.Speech.EndpointId);
            Cognitive.Configuration.ConfigureLanguageUnderstanding(azure.LanguageUnderstanding.AppId,
                azure.LanguageUnderstanding.Endpoint, azure.LanguageUnderstanding.EndpointKey);

            // Configure Discord

            var discord = botConfig.Discord;
            Discord.Configuration.Configure(discord.Token, discord.CommandsEnabled);
        }

        public void CreateGameServers(CancellationToken stoppingToken)
        {
            StoppingToken = stoppingToken;
            foreach (var gameServerConfig in botConfig.GameServers)
            {
                _gameServers.Add(gameServerFactory.CreateGameServer(gameServerConfig, botConfig.Discord.Server, stoppingToken));
            }
        }

        public async Task RunAsync()
        {
            _logger.Info($"{_name} Bot Starting");

            foreach (var gameServer in _gameServers)
                _botTasks.Add(gameServer.RunAsync());

            _botTasks.Add(StartStatusUpdateLoop(StoppingToken));
            _botTasks.Add(StartCommandProcessingLoop(StoppingToken));
            _botTasks.Add(Client.Instance.StartAsync(StoppingToken));

            await Task.WhenAll(_botTasks);
        }

        public async Task StartStatusUpdateLoop(CancellationToken cancellationToken)
        {
            _logger.Info("Starting Status Updater");
            while (!cancellationToken.IsCancellationRequested)
            {
                Collector.ControllerStatuses = _gameServers
                    .SelectMany(gs => gs.RadioControllers.Select(controller => controller.GetStatus())).ToList();
                await Task.Delay(1000);
            }

            _logger.Info("Stopping Status Updater");
        }

        public async Task StartCommandProcessingLoop(CancellationToken cancellationToken)
        {
            await Task.Run(() =>
            {
                _logger.Info("Starting Command Processor");
                while (!cancellationToken.IsCancellationRequested)
                {
                    if (CommandQueue.Commands.TryDequeue(out var command))
                    {
                        _logger.Debug($"{command.GetType()} command received");
                        switch (command)
                        {
                            case CommandQueue.EnableControllerCommand enableCommand:
                                var controllerToEnable = _gameServers.SelectMany(gs =>
                                        gs.RadioControllers.Select(controller => controller.GetStatus()))
                                    .First(x => x.Id == enableCommand.Id);
                                RadioController.ControllerEnabled[controllerToEnable.Id] = true;
                                break;
                            case CommandQueue.DisableControllerCommand disableCommand:
                                var controllerToDisable = _gameServers.SelectMany(gs =>
                                        gs.RadioControllers.Select(controller => controller.GetStatus()))
                                    .First(x => x.Id == disableCommand.Id);
                                RadioController.ControllerEnabled[controllerToDisable.Id] = false;
                                break;
                            default:
                                _logger.Debug("No action taken");
                                break;
                        }
                    }

                    Thread.Sleep(1000);
                }

                _logger.Info("Stopping Command Processor");
            }, CancellationToken.None);
        }
    }
}