﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System.Threading;
using Microsoft.CognitiveServices.Speech.Audio;
using NAudio.Wave;

namespace RurouniJones.OverlordBot.Cognitive.SpeechRecognition.Util
{
    public class BufferedWaveProviderStreamReader : PullAudioInputStreamCallback
    {
        private BufferedWaveProvider _provider;

        public BufferedWaveProviderStreamReader(BufferedWaveProvider provider)
        {
            _provider = provider;
        }

        public override int Read(byte[] dataBuffer, uint size)
        {
            // PullAudioInputStreamCallback classes are expect to block on read 
            // however BufferedWaveProvider do not. therefore we will block until
            // the BufferedWaveProvider has something to return.
            while (_provider.BufferedBytes == 0) { Thread.Sleep(50); }
            return _provider.Read(dataBuffer, 0, (int)size);
        }

        protected override void Dispose(bool disposing)
        {
            _provider.ClearBuffer();
            _provider = null;
            base.Dispose(disposing);
        }
    }
}