﻿// TextalordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System.Runtime.InteropServices;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RurouniJones.OverlordBot.Core.Awacs;
using RurouniJones.OverlordBot.Core.Responders;
using RurouniJones.OverlordBot.Datastore;
using RurouniJones.OverlordBot.GrpcDatastore;

namespace RurouniJones.TextalordBot.Console
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                if (OperatingSystem.IsWindows() && Environment.UserInteractive)
                    ConsoleProperties.DisableQuickEdit();

                Environment.CurrentDirectory = AppDomain.CurrentDomain.BaseDirectory;

                CreateHostBuilder(args).Build().Run();
            }
            catch (Exception ex)
            {
                if (!Environment.UserInteractive) throw;
                System.Console.WriteLine("Could not start TextalordBot");
                System.Console.WriteLine(ex);

                if (Environment.UserInteractive)
                {
                    System.Console.ReadKey();
                }
                throw;
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            var builder = Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddHostedService<Worker>();
                    services.AddSingleton<DcsServer>();
                    services.AddTransient<RadioMenuManager>();
                    services.AddTransient<IAirfieldRepository, AirfieldRepository>();
                    services.AddTransient<IPlayerRepository, PlayerRepository>();
                    services.AddTransient<IUnitRepository, UnitRepository>();
                    services.AddTransient<IDataSource, DataSource>();
                    services.AddSingleton<ResponderFactory>();
                    services.AddSingleton<MonitorFactory>();
                    services.AddTransient<IResponder, AwacsResponder>();
                    services.Configure<HostOptions>(
                        opts => opts.ShutdownTimeout = TimeSpan.FromSeconds(60));
                    services.AddOptions();

                });
            if (OperatingSystem.IsWindows())
                builder.UseWindowsService();

            return builder;
        }

        // https://stackoverflow.com/questions/13656846/how-to-programmatic-disable-c-sharp-console-applications-quick-edit-mode
        internal static class ConsoleProperties
        {

            // STD_INPUT_HANDLE (DWORD): -10 is the standard input device.
            private const int StdInputHandle = -10;

            private const uint QuickEdit = 0x0040;

            [DllImport("kernel32.dll", SetLastError = true)]
            private static extern IntPtr GetStdHandle(int nStdHandle);

            [DllImport("kernel32.dll")]
            private static extern bool GetConsoleMode(IntPtr hConsoleHandle, out uint lpMode);

            [DllImport("kernel32.dll")]
            private static extern bool SetConsoleMode(IntPtr hConsoleHandle, uint dwMode);

            internal static bool DisableQuickEdit()
            {

                var consoleHandle = GetStdHandle(StdInputHandle);

                GetConsoleMode(consoleHandle, out var consoleMode);

                consoleMode &= ~QuickEdit;

                return SetConsoleMode(consoleHandle, consoleMode);
            }
        }
    }

}
